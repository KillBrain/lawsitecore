<?php
	
	require_once(__DIR__.'/../../include/config.php');

    header('Content-Type: application/json; charset='.ENCODING);
    
    require_once(__DIR__.'/constants.php');
    require_once(__DIR__.'/messages.php');
    require_once(__DIR__.'/functions.php');

    $response = array(
		'status'  => STATUS_ERRORS,
		'errors'  => array(array(), $messages['unhandled_request']),
		'message' => null,
    );

    if (array_key_exists('addmessage', $_POST) && $_POST['addmessage'] == 1) {
        $response = handle_comment();
    }

    print json_encode($response);

?>