<?php
	
	$messages['success_message']       = 'Сообщение отправлено на модерацию.';
	$messages['comments_locked']       = 'Fatal error! Reboot imidiately!';
	$messages['unknown_error']         = 'Непредвиденная ошибка, повторите запрос позже.';
	$messages['type_all_fields']       = 'Заполните все поля!';
	$messages['html_and_bb_forbidden'] = 'HTML и BBCODE запрещены!';
	$messages['wrong_comment_page']    = 'Неправильная страница комментария, обновите страницу и повторите действие.';
	$messages['captcha_invalid']       = 'Ошибка капчи, повторите попытку.';
	$messages['unhandled_request']     = 'Необработанный запрос.';

?>