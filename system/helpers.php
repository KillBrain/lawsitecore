<?php if (!defined('CONFIG')) die('Hacking attempt!');

	/*
		Format date
	*/
	function format_date($date) {
		$date = explode('-', $date);
		
		$y = $date[0];
		
		switch($date[1]){
            case "01":
                $m = "января";
                break;
            case "02":
                $m = "февраля";
                break;
            case "03":
                $m = "марта";
                break;
            case "04":
                $m = "апреля";
                break;
			case "05":
                $m = "мая";
                break;
            case "06":
                $m = "июня";
                break;
            case "07":
                $m = "июля";
                break;
			case "08":
                $m = "августа";
                break;
			case "09":
                $m = "сентября";
                break;
			case "10":
                $m = "октября";
                break;
			case "11":
                $m = "ноября";
                break;
			case "12":
                $m = "декабря";
                break;
        }
		
		if (substr($date[2], 0, 1) != 0){
			$d = $date[2];
		} else {
			$d = substr($date[2], 1, 1);
		}
		
		$date = $d.' '.$m.' '.$y.' г.';

		return $date;
	}
	
	/*
		Print param if param not empty
	*/
	function print_param($param, $else = '') {
		return print_text_param($param, $param, $else);
	}

	/*
		Print text if param not empty
	*/
	function print_text_param($param, $text, $else = '') {
		if (is_array($param)) {
			if (!array_key_exists($param[1], $param[0])) {
				return $else;
			} else {
				$key  = $param[1];
				$obj  = $param[0];
				$text = $obj[$key];
			}
		}
		
		return (!empty($param)) ? $text : $else;
	}

?>