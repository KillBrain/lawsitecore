<?php if (!defined('CONFIG')) die('Hacking attempt!');

	// link to database connection
	$link = null;
	/*
		Connect to database, if this not do
	*/
	function connect_db() {
		global $link;

		if (empty($link)) {
			mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

			$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
			$link->set_charset(DB_CHARSET);
			$link->options(MYSQLI_OPT_CONNECT_TIMEOUT, 600);
			$link->query("SET time_zone = '" . DB_TIMEZONE . "'");
		}
	}

	/*
		MySQL sql-handler; all non-mysqli_sql_exception throws
	*/
	function display_db_error($e) {
		if (!is_a($e, 'mysqli_sql_exception')) {
			throw $e;
		} else {
			if (DEBUG) {
				print "MySQL Error: ".$e->getMessage();
			} else {
				print 'Произошла ошибка при обращении к базе данных. Пожалуйста, сообщите нашему технику об ошибке :)';
			}
			die();
		}
	}

	set_exception_handler('display_db_error');
	connect_db();

?>