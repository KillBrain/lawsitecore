<?php if (!defined('CONFIG')) die('Hacking attempt!');

	require_once(SYSTEM_DIR.'/functions.php');

	$allowed_img_types = array(
		'image/gif'  => '.gif',
		'image/jpeg' => '.jpg',
		'image/png'  => '.png',
	);

	/*
		Upload and save img. Return url
	*/
	function upload_img($img, $storage_dir, $storage_url, $width, $height) {
		global $allowed_img_types;

		if (!is_writable($storage_dir)) {
			return null;
		}

		if (!is_uploaded_file($img['tmp_name'])) {
			return null;
		}

		if (!in_array($img['type'], array_keys($allowed_img_types))) {
			return null;
		}

		$file_addr = random_filename($storage_dir, $allowed_img_types[$img['type']]);

        if (false === move_uploaded_file($img["tmp_name"], $file_addr)) {
            return null;
        }

        resize_img($file_addr, $img['type'], $width, $height);

        $file_addr = $storage_url.'/'.basename($file_addr);

        return $file_addr;
	}

	/*
		Load from remote and save img. Return url
	*/
	function reload_img($img_url, $storage_dir, $storage_url, $width = null, $height = null) {
		global $allowed_img_types;

		if (!is_writable($storage_dir)) {
			return null;
		}

		$img = load_file($img_url, $storage_dir, $allowed_img_types);

		if (is_null($img)) {
			return null;
		}

		resize_img($img['name'], $img['type'], $width, $height);

		$img['name'] = $storage_url.'/'.basename($img['name']);

		return $img['name'];
	}

	/*
		Resize img
	*/
	function resize_img($img, $filetype, $width, $height) {
		$original_info = getimagesize($img);
		$original_w    = $original_info[0];
		$original_h    = $original_info[1];

		$original_img = null;

		switch ($filetype) {
			case 'image/gif':
				$original_img = imagecreatefromgif($img);
				break;
			case 'image/jpeg':
				$original_img = imagecreatefromjpeg($img);
				break;
			case 'image/png':
				$original_img = imagecreatefrompng($img);
				break;
		}

		if (is_null($original_img)) {
			return;
		}

		$thumb_img = imagecreatetruecolor($width, $height);

		if ($filetype == 'image/png') {
			imagealphablending($thumb_img, false);
			imagesavealpha($thumb_img, true);
		}

		imagecopyresampled($thumb_img, $original_img,
		                   0, 0,
		                   0, 0,
		                   $width, $height,
		                   $original_w, $original_h);
		
		switch ($filetype) {
			case 'image/gif':
				imagegif($thumb_img, $img);
				break;
			case 'image/jpeg':
				imagejpeg($thumb_img, $img, 75);
				break;
			case 'image/png':
				imagepng($thumb_img, $img);
				break;
		}

		imagedestroy($thumb_img);
		imagedestroy($original_img);
	}

?>