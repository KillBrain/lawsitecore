<?php

	require_once(__DIR__.'/../../include/config.php');
	require_once(SYSTEM_DIR.'/helpers.php');
	require_once(SYSTEM_DIR.'/auth/functions.php');
    require_once(SYSTEM_DIR.'/comments/functions.php');
    require_once(ADMIN_DIR.'/lib/constants.php');
    require_once(ADMIN_DIR.'/lib/functions.php');
    require_once(ADMIN_DIR.'/lib/helpers.php');

    $caption            = 'Доступ запрещен!';
    $content_template   = 'no_permissions';
    $new_comments_count = get_new_comments_count();

    include(ADMIN_TEMPLATES_DIR.'/layout.tpl');

?>