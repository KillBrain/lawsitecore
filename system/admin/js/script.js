var ua = navigator.userAgent.toLowerCase();
var isOpera = (ua.indexOf('opera')  > -1);
var isIE = (!isOpera && ua.indexOf('msie') > -1);
var js_show_window_state = 0;
var pics = new Array();
var pics_count = 0;
var aaa = 0;

function getBodyScrollTop(){
    return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
}

function getDocumentHeight(){ // высота всей страницы вместе с невидимой частью
        return Math.max(document.compatMode!='CSS1Compat'?document.body.scrollHeight:document.documentElement.scrollHeight,getViewportHeight());
}

function getDocumentWidth(){ // ширина всей страницы вместе с невидимой частью
        return Math.max(document.compatMode!='CSS1Compat'?document.body.scrollWidth:document.documentElement.scrollWidth,getViewportWidth());
}

function getViewportHeight(){ // высота браузера
        return ((document.compatMode||isIE)&&!isOpera)?(document.compatMode=='CSS1Compat')?document.documentElement.clientHeight:document.body.clientHeight:(document.parentWindow||document.defaultView).innerHeight;
}

function getViewportWidth(){ // ширина браузера
        return ((document.compatMode||isIE)&&!isOpera)?(document.compatMode=='CSS1Compat')?document.documentElement.clientWidth:document.body.clientWidth:(document.parentWindow||document.defaultView).innerWidth;
}

function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}

function setElementOpacity(sElemId, nOpacity)
{
  var opacityProp = getOpacityProperty();
  var elem = document.getElementById(sElemId);

  if (!elem || !opacityProp) return; // Если не существует элемент с указанным id или браузер не поддерживает ни один из известных функции способов управления прозрачностью

  if (opacityProp=="filter")  // Internet Exploder 5.5+
  {
    nOpacity *= 100;

    // Если уже установлена прозрачность, то меняем её через коллекцию filters, иначе добавляем прозрачность через style.filter
    var oAlpha = elem.filters['DXImageTransform.Microsoft.alpha'] || elem.filters.alpha;
    if (oAlpha) oAlpha.opacity = nOpacity;
    else elem.style.filter += "progid:DXImageTransform.Microsoft.Alpha(opacity="+nOpacity+")"; // Для того чтобы не затереть другие фильтры используем "+="
  }
  else // Другие браузеры
    elem.style[opacityProp] = nOpacity;
}

function getOpacityProperty()
{
  if (typeof document.body.style.opacity == 'string') // CSS3 compliant (Moz 1.7+, Safari 1.2+, Opera 9)
    return 'opacity';
  else if (typeof document.body.style.MozOpacity == 'string') // Mozilla 1.6 и младше, Firefox 0.8
    return 'MozOpacity';
  else if (typeof document.body.style.KhtmlOpacity == 'string') // Konqueror 3.1, Safari 1.1
    return 'KhtmlOpacity';
  else if (document.body.filters && navigator.appVersion.match(/MSIE ([\d.]+);/)[1]>=5.5) // Internet Exploder 5.5+
    return 'filter';

  return false; //нет прозрачности
}

function js_show_window(content){
    if(js_show_window_state == 0){
        js_show_window_state = 1;
        var a = document.createElement("div");
var wheight=(window.innerHeight)?window.innerHeight:
     ((document.all)?document.body.offsetHeight:null);
        a.id = "a_img";
        a.style.position = "absolute";
        a.style.left = "0px";
        a.style.top = "0px";
        a.style.height = wheight + "px";
        a.style.right = "0px";
        a.style.zIndex = "100";
        a.innerHTML= content;
        a.style.top = ((getBodyScrollTop()-10)+"px");
        setElementOpacity("maket", 0.4);
        document.body.appendChild(a);
        js_dinamic_show_obj = document.getElementById("aa_img");
        window.onscroll = function () {a.style.top = ((getBodyScrollTop()-10)+"px");};
        window.onresize= function(){
                a.style.width= (document.body.clientWidth-20) + "px";
                a.style.height= (document.body.clientHeight-20) + "px";
        };
    }else{
        js_show_window_state  = 0;
        document.body.removeChild(document.getElementById("a_img"));
        setElementOpacity("maket", 1.0);
    }
}

function add_pick(a_img){
    pics[pics_count] = a_img;
    pics_count = pics_count+1;
}

function next_img(form){
    for(var i = 0; i < pics_count; i++){
        if("http://.*"+pics[i] == form.src || pics[i] == form.src){
            if(i == pics_count-1){
                form.src=pics[0];
                return;
            }else{
                form.src=pics[i+1];
            }return;
        }
    }js_show_window();
    return;
}

function js_show_img(a_img){
        js_show_window('<table width=100% height=100% border=0 style="border: 0px;"><tr><td align=center valign=middle onClick="if(js_show_window_state == 0) js_show_window_state = 1; else js_show_window();" ><img id="aa_img"  style="max-width: 800px; max-height: 600px; opacity: 1.0; filter:alpha(opacity=1); border:1px solid #dadada; padding: 7px; background: #e6e6e6;" src="'+a_img+'" onClick="js_show_window_state = 0;  if(pics_count>1){aaa=1;next_img(this);}"></td></tr></table>');
}


function contact_us(){
    var s = prompt("Укажите свой номер телефона:", "+7");
    var req = getXmlHttp();
    if (req && s != "+7" && s != null && s != "" && s != " " && s.length > 5) {
        req.onreadystatechange = function() {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    var t = req.responseText;
                    if(t == "1"){
                        alert("Спасибо за Ваше обращение!\n\nМы свяжемся с Вами в ближайшее время.\n");
                    }
                }
            }
        }
        req.open("GET", "/ajax.php?phone="+s, true);
        req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        req.send();
    }else{
        alert("Заказ звонка не выполнен.\n\nПопробуйте еще раз.\n");
    }
}

function returnDate(date){
    alert(date);
}

function generatePassword() {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}