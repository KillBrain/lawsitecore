<?php

	require_once(__DIR__.'/../../include/config.php');
	require_once(SYSTEM_DIR.'/helpers.php');
    require_once(SYSTEM_DIR.'/auth/functions.php');
	require_once(SYSTEM_DIR.'/comments/functions.php');
    require_once(ADMIN_DIR.'/lib/functions.php');
    require_once(ADMIN_DIR.'/lib/constants.php');

    check_auth(ADMIN_LOGIN_URL);
    check_admin(ADMIN_NO_PERMISSIONS);

    if (array_key_exists('save', $_POST)) {
        $block   = $_POST['block'];
        $comment = $_POST['comment'];
        $answer  = $_POST['answer'];

        save_template_code('block', $block);
        save_template_code('comment', $comment);
        save_template_code('answer', $answer);
    }

    $caption            = 'Шаблоны';
    $content_template   = 'templates';
    $new_comments_count = get_new_comments_count();
    $block              = get_template_code('block');
    $comment            = get_template_code('comment');
    $answer             = get_template_code('answer');

    include(ADMIN_TEMPLATES_DIR.'/layout.tpl');

?>