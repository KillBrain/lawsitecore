<? if (count($moders) > 0): ?>
	<? foreach ($moders as $item): ?>
		<table cellpadding="10" cellspacing="0" class="font_2 non-full" width="300">
			<tr>
				<td>
					<img src="<?=$item['img'];?>" />
				</td>
				<td width=100%>
					<b><?=$item['name'];?></b><br/>
					Комментариев: <?=$item['comments'];?><br/>
					<a href="<?=sprintf(ADMIN_MODERS_DELETE_URL, $item['id']);?>" onClick="return confirm('Вы действительно хотите удалить эту запись?');">Удалить</a>
					<hr>
				</td>
			</tr>
		</table>
	<? endforeach; ?>
<? else: ?>
	<p>Модераторов нет.</p>
<? endif; ?>
<br />
<div style="width: 300px; text-align: left;">Добавить модератора:<br/><br/></div>
<? if (isset($error)): ?>
<p><?=$error;?></p>
<? endif; ?>
<form method="POST" ENCTYPE="multipart/form-data">
    <table cellpadding="10" cellspacing="0" class="font_2 non-full" width="300">
	    <tr>
		    <td style="border-right: #3d85be 1px solid; border-bottom: #3d85be 1px solid;" align="left">Имя</td>
		    <td style=" border-bottom: #3d85be 1px solid;"><input type="text" name="name" /></td>
	    </tr>
	    <tr>
		    <td style="border-right: #3d85be 1px solid; border-bottom: #3d85be 1px solid;" align="left">Пароль</td>
		    <td style=" border-bottom: #3d85be 1px solid;"><input type="password" name="pass" id="pass" />&nbsp;<button type="button" onclick="genPass();">Генерировать</button></td>
	    </tr>
	    <tr>
		    <td style="border-right: #3d85be 1px solid;">Аватарка</td>
		    <td><input type="file" name="<?=ADMIN_MODER_IMG_NAME;?>" /> (только .gif; .jpg; .png)<br/>или<br/><input type="text" name="url" value="" /></td>
	    </tr>
	    <tr>
		    <td colspan="2" style="border-top: #3d85be 1px solid;" align="center"><input type="submit" name="add" value="Отправить" /></td>
	    </tr>
    </table>
</form>
<script>
	function genPass() {
		var id = document.getElementById('pass');
		var pass = generatePassword();

		if (prompt('Установить этот пароль?', pass)) {
			id.value = pass;
		}
	}
</script>