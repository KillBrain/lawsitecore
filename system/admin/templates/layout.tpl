<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
	    <title><?=ADMIN_PANEL_NAME;?></title>
	    <meta http-equiv="content-type" content="text/html; charset=<?=ENCODING;?>" />
	    <link href="<?=ADMIN_HOME.'/css/style.css';?>" rel="stylesheet">
	<? if (isset($datepicker)): ?>
	<link href="<?=ADMIN_HOME.'/css/jquery-ui.min.css';?>" rel="stylesheet">
	<? endif; ?>
	</head>
	<body>
	    <table id="main">
	        <tr>
	            <td class="header">
	                <table>
	                    <tr>
	                        <td class="logo"><h1 class="font_1"><a href="<?=ADMIN_HOME_URL;?>"><?=ADMIN_PANEL_NAME;?></a></h1></td>
	                    </tr>
	                </table>
	            </td>
	        </tr>
	        <tr>
	            <td>
	                <table class="body">
	                    <tr>
	                    <? if (is_auth()): ?>
                            <td class="sidebar">
                            <? include(ADMIN_TEMPLATES_DIR.'/menu.tpl'); ?>
                            </td>
	                    <? else: ?>
	        				<td width="20">&nbsp;</td>
	        			<? endif; ?>
	                    <td class="content">
	                  		<? if (is_auth()): ?>
	                            <table>
	                                <tr>
	                                	<td height="30">
	                            			<h1 class="font_4"><?=$caption;?></h1><br />
	                                    </td>
	                                </tr>
                                    <tr>
                                    	<td align="center">
                                            <center>
                                            	<font class="font_5">
                            						<? include(ADMIN_TEMPLATES_DIR.'/'.$content_template.'.tpl'); ?>
                                                </font>
                                            </center>
                                    	</td>
                                    </tr>
	                            </table>
	                        <? else: ?>
	                        	<table>
	                        		<tr>
	                                	<td height="30">
	                            			<h1 class="font_4">Авторизация</h1><br />
	                                    </td>
	                                </tr>
				                	<tr>
				                		<td align="center" valign="middle">
							                <? include(ADMIN_TEMPLATES_DIR.'/login_form.tpl'); ?>
				                		</td>
				                	</tr>
				                </table>
				            <? endif; ?>
	                        </td>
	                    </tr>
	                </table>
	            </td>
	        </tr>
	        <tr>
	            <td class="footer">
	            	<p>Здесь был копирайт прошлого разработчика, но я его специально удалил :)</p>
	            </td>
	        </tr>
	    </table>

	    <script src="<?=ADMIN_HOME.'/js/jquery-1.11.0.min.js';?>" type="text/javascript"></script>
	<? if (isset($datepicker)): ?>
	<script src="<?=ADMIN_HOME.'/js/jquery-ui.min.js';?>" type="text/javascript"></script>
	<script src="<?=ADMIN_HOME.'/js/i18n/datepicker-ru.js';?>" type="text/javascript"></script>
	<script>
		$(document).ready(function() {
			$('.datepicker').datepicker({
				dateFormat: "yy-mm-dd"
			});
		});
	</script>
	<? endif; ?>
	<script src="<?=ADMIN_HOME.'/js/script.js';?>" type="text/javascript"></script>
	</body>
</html>