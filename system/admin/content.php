<?php

	require_once(__DIR__.'/../../include/config.php');
    require_once(SYSTEM_DIR.'/helpers.php');
	require_once(SYSTEM_DIR.'/functions.php');
    require_once(SYSTEM_DIR.'/auth/functions.php');
    require_once(SYSTEM_DIR.'/rating/functions.php');
    require_once(SYSTEM_DIR.'/comments/functions.php');
    require_once(SYSTEM_DIR.'/date/functions.php');
    require_once(SYSTEM_DIR.'/comments/helpers.php');
    require_once(SYSTEM_DIR.'/moders/functions.php');
	require_once(SYSTEM_DIR.'/moders/helpers.php');
    require_once(ADMIN_DIR.'/lib/functions.php');
    require_once(ADMIN_DIR.'/lib/constants.php');
    require_once(ADMIN_DIR.'/lib/helpers.php');

    check_auth(ADMIN_LOGIN_URL);

    if (array_key_exists('add', $_POST)) {
        // добавление фейкового коммента
        $name        = trim(strip_tags($_POST['name']));
        $img         = trim(strip_tags($_POST['img']));
        $text        = trim(strip_tags($_POST['text']));
        $moder_id    = (int)$_POST['moder_id'];
        $answer      = trim(strip_tags($_POST['answer']));
        $date        = $_POST['date'];
        $content_id  = (int)$_POST['content_id'];
        $content     = get_content_by_id($content_id);
        $content_url = null;

        if (is_null($content)) {
            redirect_to(sprintf(ADMIN_CONTENT_ITEM_URL, $content_id));
            die();
        } else {
            $content_url = $content['url'];
        }

        unset($content);

        if (empty($img)) {
            $img = get_random_avatar();
        }

        if (!is_admin()) {
            $moder    = get_moder();
            $moder_id = $moder['id'];

            unset($moder);
        }

        $tmp = strtotime($date);

        if (false === $tmp) {
            $date = null;
        } else {
            $date = date('Y-m-d', $tmp);
        }

        if (!is_null($content_url)) {
            add_fake_comment($content_url, $name, $img, $text, $moder_id, $answer, $date);
        }

        unset($tmp);
        redirect_to(sprintf(ADMIN_CONTENT_ITEM_URL, $content_id));
        die();
    }

    if (array_key_exists('moderate', $_POST)) {
        // модерирование коммента
        $name          = trim(strip_tags($_POST['name']));
        $img           = trim(strip_tags($_POST['img']));
        $text          = trim(strip_tags($_POST['text']));
        $moder_id      = (int)$_POST['moder_id'];
        $answer        = trim(strip_tags($_POST['answer']));
        $date          = $_POST['date'];
        $comment_id    = (int)$_POST['comment_id'];
        $redirect      = $_POST['redirect'];
        $comment       = get_comment_by_id($comment_id);
        $current_moder = get_moder();

        if (empty($img)) {
            $img = ANON_DEFAULT_IMG;
        }

        // если коммент не текущего модера
        if (!is_admin() && !in_array($comment['moder_id'], array($current_moder['id'], -777))) {
            redirect_to($redirect);
            die();
        }

        $tmp = strtotime($date);

        if (false === $tmp) {
            $date = null;
        } else {
            $date = date('Y-m-d', $tmp);
        }

        update_comment($comment_id, $name, $img, $text, $moder_id, $answer, $date);

        unset($tmp);

        if (empty($redirect)) {
            redirect_to(ADMIN_CONTENT_URL);
        } else {
            redirect_to($redirect);
        }

        die();
    }

    if (array_key_exists('id', $_GET) && array_key_exists('delete', $_GET)) {
        // удалить рейтинг, комменты, дату и сделать редирект на страницу дат статей
        $content_date = get_content_date_by_id((int)$_GET['id']);

        // если текущий пользователь админ или ответчик коммента
        if (is_admin()) {
            delete_content_date_by_url($content_date['url']);
            delete_comments($content_date['url']);
            delete_rating_by_url($content_date['url']);
        }

        if (empty($_SERVER['HTTP_REFERER'])) {
            redirect_to(ADMIN_CONTENT_URL);
        } else {
            redirect_to($_SERVER['HTTP_REFERER']);
        }

        die();
    }

    if (array_key_exists('comment_id', $_GET) && array_key_exists('delete', $_GET)) {
        // удалить коммент и сделать редирект на страницу комментов статьи
        $comment    = get_comment_by_id((int)$_GET['comment_id']);
        $moder      = get_moder();

        // если текущий пользователь админ или ответчик коммента
        if (is_admin()
            || (!is_admin() && $comment['moder_id'] == $moder['id'])) {
            delete_comment($comment['id']);

            if ($comment['moder_id'] != -777) {
                update_moder_comments($comment['moder_id'], -1);
            }
        }

        if (empty($_SERVER['HTTP_REFERER'])) {
            redirect_to(ADMIN_CONTENT_URL);
        } else {
            redirect_to($_SERVER['HTTP_REFERER']);
        }

        die();
    }

    $caption            = 'Статьи';
    $content_template   = 'content_list';
    $new_comments_count = get_new_comments_count();

    if (array_key_exists('id', $_GET)) {
        // показать список комментов статьи
        $content_template = 'content_comments';
        $content_id       = (int)$_GET['id'];
        $comment_id       = 0;
        $content          = get_content_by_id($content_id);
        $content_page     = null;

        if (is_null($content)) {
            redirect_to(ADMIN_CONTENT_URL);
            die();
        } else {
            $content_page = $content['url'];
        }

        unset($content);

        $comments         = get_comments_by_content_url($content_page);
        $action           = 'add';
        $name             = get_random_name();
        $img              = get_random_avatar();
        $date             = date("Y-m-d");
        $moders           = null;
        $text             = '';
        $answer           = '';
        $datepicker       = true; // включаем календарик на поле с датой
        $redirect         = '';
        $moder_id         = null;
        
        if (is_admin()) {
            $moders = get_moders();
        } else {
            $moders = array(get_moder());
        }
    } else if (array_key_exists('comment_id', $_GET)) {
        $datepicker = true; // включаем календарик на поле с датой

        // показать форму модерирования коммента
        $content_template = 'content_comment_form';
        $content_id       = 0;
        $comment_id       = (int)$_GET['comment_id'];
        $comment          = get_comment_by_id($comment_id);
        $redirect         = $_SERVER['HTTP_REFERER'];
        $action           = 'moderate';
        
        if (is_admin()) {
            $moders = get_moders();
        } else {
            $moders = array(get_moder());
        }

        if (is_null($comment)) {
            if (empty($_SERVER['HTTP_REFERER'])) {
                redirect_to(ADMIN_CONTENT_URL);
            } else {
                redirect_to($_SERVER['HTTP_REFERER']);
            }

            die();
        }

        extract($comment);
    } else {
        // показать список всех статей
        $content_list = get_content_list();
    }

    include(ADMIN_TEMPLATES_DIR.'/layout.tpl');

?>