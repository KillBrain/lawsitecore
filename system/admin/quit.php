<?php

	require_once(__DIR__.'/../../include/config.php');
	require_once(SYSTEM_DIR.'/auth/functions.php');
    require_once(ADMIN_DIR.'/lib/constants.php');

    logout_user(ADMIN_HOME_URL);

?>