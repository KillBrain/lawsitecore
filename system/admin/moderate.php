<?php

	require_once(__DIR__.'/../../include/config.php');
	require_once(SYSTEM_DIR.'/helpers.php');
    require_once(SYSTEM_DIR.'/auth/functions.php');
    require_once(SYSTEM_DIR.'/comments/functions.php');
	require_once(SYSTEM_DIR.'/comments/helpers.php');
    require_once(ADMIN_DIR.'/lib/functions.php');
    require_once(ADMIN_DIR.'/lib/helpers.php');
    require_once(ADMIN_DIR.'/lib/constants.php');

    check_auth(ADMIN_LOGIN_URL);

    $comments_enabled = !has_comments_lock();

    if (array_key_exists('comments_enable', $_GET) && in_array($_GET['comments_enable'], array(0, 1))) {
        $comments_enabled = (bool)$_GET['comments_enable'];

        if ($comments_enabled && has_comments_lock()) {
            unlock_comments();
        } else {
            lock_comments();
        }

        redirect_to(ADMIN_MODERATE_URL);
        die();
    }

    $caption            = 'Модерация сообщений';
    $content_template   = 'moderate';
    $new_comments_count = get_new_comments_count();
    $comments           = get_unchecked_comments();

    include(ADMIN_TEMPLATES_DIR.'/layout.tpl');

?>