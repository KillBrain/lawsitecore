<?php if (!defined('CONFIG')) die('Hacking attempt!');

	require_once(SYSTEM_DIR.'/functions.php');
	require_once(SYSTEM_DIR.'/db_functions.php');

	/*
		Return stat fields
	*/
	function get_stat_variables() {
		global $link;
		// combine all counters, the dates of last and first comment
		$sql = '(SELECT COUNT(*), null FROM `comments`)
				UNION ALL
				(SELECT COUNT(*), null FROM `comments` WHERE `date` <= NOW())
				UNION ALL
				(SELECT COUNT(*), null FROM `comments` WHERE `date` = NOW())
				UNION ALL
				(SELECT COUNT(*), null FROM `comments` WHERE `date` > NOW())
				UNION ALL
				(SELECT null, IFNULL((SELECT date FROM `comments` ORDER BY `date` DESC LIMIT 0, 1), null))
				UNION ALL
				(SELECT null, IFNULL((SELECT date FROM `comments` ORDER BY `date` ASC LIMIT 0, 1), null))';
		$result = $link->query($sql);

		// in php 5.3 expression fetch_array()[0] is syntax error so...
		$total_count          = $result->fetch_array(); $total_count          = (int)$total_count[0];
		$total_posted_count   = $result->fetch_array(); $total_posted_count   = (int)$total_posted_count[0];
		$today_posted_count   = $result->fetch_array(); $today_posted_count   = (int)$today_posted_count[0];
		$today_unposted_count = $result->fetch_array(); $today_unposted_count = (int)$today_unposted_count[0];
		$last_comment_date    = $result->fetch_array(); $last_comment_date    = $last_comment_date[1];
		$first_comment_date   = $result->fetch_array(); $first_comment_date   = $first_comment_date[1];
		$comments_intensity   = 0;

		unset($sql, $result);

		// calc intensity
		if ($total_count < 2) {
			$comments_intensity = $total_count;
		} else {
			$interval = (int)date_diff(
				date_create($last_comment_date),
				date_create($first_comment_date),
				true
			)->format('%a');

			if ($interval > 0) {
				$comments_intensity = round($total_count / $interval, 2);
			}
		}

		return array(
			'total_count'          => $total_count,
			'total_posted_count'   => $total_posted_count,
			'today_posted_count'   => $today_posted_count,
			'today_unposted_count' => $today_unposted_count,
			'last_comment_date'    => $last_comment_date,
			'first_comment_date'   => $first_comment_date,
			'comments_intensity'   => $comments_intensity,
		);
	}

	/*
		Return content page by id
	*/
	function get_content_by_id($content_id) {
		global $link;

		$sql  = 'SELECT * FROM content_date WHERE id = ?';
		$stmt = $link->prepare($sql);

		$stmt->bind_param('i', $content_id);
		$stmt->execute();

		$result   = $stmt->get_result();
		$response = null;

		unset($sql, $stmt);

		if ($result->num_rows > 0) {
			$response = $result->fetch_assoc();
		}

		unset($result);

		return $response;
	}

	/*
		Return content pages list
	*/
	function get_content_list() {
		global $link;

		$sql      = 'SELECT * FROM content_date ORDER BY url ASC';
		$result   = $link->query($sql);
		$response = array();

		if ($result->num_rows > 0) {
			$response = $result->fetch_all(MYSQLI_ASSOC);
		}

		unset($sql, $result);

		return $response;
	}

	/*
		Return content pages with sort
	*/
	function get_content_list_sort($sort) {
		global $link;

		$sql = 'SELECT * FROM content_date ORDER BY date ';

		if ($sort == 'asc') {
			$sql .= 'ASC';
		} else {
			$sql .= 'DESC';
		}

		$result   = $link->query($sql);
		$response = array();

		if ($result->num_rows > 0) {
			$response = $result->fetch_all(MYSQLI_ASSOC);
		}

		unset($sql, $result);

		return $response;
	}

	/*
		Update content page date
	*/
	function update_content_date($content_id, $date) {
		global $link;

		$sql = 'UPDATE `content_date` SET `date` = ? WHERE id = ?';
		$stmt = $link->prepare($sql);
		$stmt->bind_param('sd', $date, $content_id);
		$stmt->execute();

		$affected_rows = $stmt->affected_rows;

		unset($sql, $stmt);

		return ($affected_rows > 0);
	}

	/*
		Update core
	*/
	function update_core() {
		if (is_dir(ADMIN_UPDATE_CACHE_DIR)) {
			// clean cache dir
			rrmdir(ADMIN_UPDATE_CACHE_DIR);
		} else {
			mkdir(ADMIN_UPDATE_CACHE_DIR, 0777, true);
		}

		$log = 'Init downloading.<br>';

		$update_name             = md5(uniqid());
		$_SESSION['update_name'] = $update_name;
		$update_file             = ADMIN_UPDATE_CACHE_DIR.'/'.$update_name.'.zip';
		$extract_path            = ADMIN_UPDATE_CACHE_DIR.'/'.$update_name;
		$update_url              = ADMIN_UPDATE_ARCH;

		$zip_resource = fopen($update_file, 'w+');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $update_url);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
		curl_setopt($ch, CURLOPT_FILE, $zip_resource);
		$page = curl_exec($ch);

		if (false === $page) {
			$log .= 'Download zip fail.<br>';
			return $log;
		}

		curl_close($ch);

		$log .= 'Download zip success!<br>';

		$zip = new ZipArchive();
		
		if (true != $zip->open($update_file)) {
			$log .= 'Unable to open zip file.<br>';
			return $log;
		}

		$zip->extractTo($extract_path);
		$zip->close();

		$log .= 'File unzipped!<br>';
		$log .= 'Removing files in ' . SYSTEM_DIR . '<br>';

		rrmdir(SYSTEM_DIR, false);

		$log .= 'Moving files to system dir.<br>';

		$dirs    = scandir($extract_path);
		$old_dir = $extract_path.'/'.$dirs[2].'/system';

		if (!is_dir($old_dir)) {
			$log .= 'No extract dir ' . $old_dir . '<br>';
			return $log;
		}

		rename($old_dir, SYSTEM_DIR);

		$log .= 'Done!<br>';

		return $log;
	}

?>