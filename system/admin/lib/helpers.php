<?php if (!defined('CONFIG')) die('Hacking attempt!');

	/*
		Render comments stat
	*/
	function render_comments_stat($comments_stat) {
		$html = '';

		if (count($comments_stat) > 0) {
	    	foreach ($comments_stat as $item) {
		        $html .= '<tr>'.
		    				'<td class="font_2" style=" border-right: #c0c0c0 1px solid; border-top:#c0c0c0 1px solid;">'.
		    					'<p>'.
		    						'<a href="' . sprintf(ADMIN_STAT_COMMENTS_URL, $item['date']) . '">' . date('d.m.Y', strtotime($item['date'])) . '</a>'.
		    					'</p>'.
		    				'</td>'.
		    				'<td style=" border-top: #c0c0c0 1px solid;">' . $item['count'] . '</td>'.
						  '</tr>';
			}
		}

		return $html;
	}

	/*
		Render content list
	*/
	function render_content_list($content_list, $edit_url_format = ADMIN_CONTENT_ITEM_URL, $delete_url_format = ADMIN_CONTENT_ITEM_DELETE_URL) {
		$html = '';

		if (count($content_list) > 0) {
			$html = '<ul class="ul_article" align="left">';

			foreach ($content_list as $content) {
				$html .= '<li>'.
							'Дата: '.$content['date'].' - '.
							'<a href="' . sprintf($edit_url_format, $content['id']) . '">'.
								$content['url'].
							'</a>'.
							'&nbsp;[<a href="' . sprintf($delete_url_format, $content['id']) . '" style="color:#f00;" onclick="return confirm(\'Вы действительно хотите удалить эту статью?\');">Удалить</a>]'.
						  '</li>';
			}

			$html .= '</ul>';
		} else {
			$html = '<p>Нет статей.</p>';
		}

		return $html;
	}

?>