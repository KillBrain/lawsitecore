<?php

	require_once(__DIR__.'/../../include/config.php');
	require_once(SYSTEM_DIR.'/helpers.php');
	require_once(SYSTEM_DIR.'/auth/functions.php');
    require_once(ADMIN_DIR.'/lib/messages.php');
    require_once(ADMIN_DIR.'/lib/constants.php');

    if (is_auth()) {
        redirect_to(ADMIN_HOME_URL);
        die();
    }

    if (array_key_exists('key', $_POST)) {
        $key = $_POST['key'];

        if (false === login_user($key, ADMIN_HOME_URL)) {
            $error = $messages['login_error'];
        }
    }

    $caption = 'Статистика';

    include(ADMIN_TEMPLATES_DIR.'/layout.tpl');

?>