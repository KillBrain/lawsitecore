<?php if (!defined('CONFIG')) die('Hacking attempt!');
    
    require_once(SYSTEM_DIR.'/functions.php');
    require_once(SYSTEM_DIR.'/helpers.php');
    require_once(__DIR__.'/functions.php');
	
    $page_address = get_page_address();
    $date = get_content_date($page_address);

    if(empty($date)) {
        $date = put_content_date($page_address);
    }
    
    include(__DIR__.'/date.tpl');
?>