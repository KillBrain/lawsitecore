$(document).ready(function() {
  var vote_rating = function(value) {
    var page_address = document.location.pathname;
    if (page_address.substr(-1) == '/') {
      page_address += 'index.php';
    }
    var params = {
      'value':value,
      'page_address':page_address
    };
    $.get("/system/rating/ajax.php", params, function(result) {
      $("#rating").html(result[0]);
      $("#count").html(result[1]);
    });
  };

  var lock_rating = function() {
    $("#mainvote").addClass('voted');
  };

  var check_rating_cookie = function() {
    if (typeof cookie_name === 'undefined') {
      cookie_name = 'voted';
    }
    var voted = $.cookie(cookie_name);
    if (voted == 1) {
      lock_rating();
    }
  };

  $('#plus_b').click(function() {
    lock_rating();
    vote_rating('plus');
  });

  $('#minus_b').click(function() {
    lock_rating();
    vote_rating('minus');
  });  

  check_rating_cookie();
});