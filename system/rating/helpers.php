<?php if (!defined('CONFIG')) die('Hacking attempt!');

	/*
		Render rating list
	*/
	function render_rating($rating_list, $url_template = ADMIN_RATING_DELETE_URL) {
		$html = '';

		if (count($rating_list) > 0) {
			$html = '<ul class="ul_article" align="left">';

			foreach ($rating_list as $item) {
				$html .= '<li><a href="' . $item['url'] . '" target="_blank">' . $item['url'] . '</a> - Рейтинг: ' . $item['rating'] . ' - Проголосовало: ' . $item['count'] . ' - [<a href="' . sprintf($url_template, $item['content_id']) . '" style="color:#f00;" onclick="return confirm(\'Вы действительно хотите удалить эту статью?\');">Удалить</a>]</li>';
			}

			$html .= '</ul>';
		} else {
			$html = '<p>Нет статей.</p>';
		}

		return $html;
	}