<?php if (!defined('CONFIG')) die('Hacking attempt!');

    require_once(SYSTEM_DIR.'/functions.php');
    require_once(__DIR__.'/functions.php');
	
	$page_address         = get_page_address();
	list($rating, $count) = get_rating($page_address);
	$voted_cookie_name    = md5($page_address).'_voted';
	$voted                = '';

    if (array_key_exists($voted_cookie_name, $_COOKIE)){
		$voted = ' voted';
	}
	include_once(__DIR__.'/rating.tpl');
?>