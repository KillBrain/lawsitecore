<?php

	require_once(__DIR__.'/../../include/config.php');

	header('Content-Type: application/json; charset='.ENCODING);

    require_once(__DIR__.'/functions.php');

	if (!array_key_exists('value', $_GET) || !array_key_exists('page_address', $_GET)) {
		print '[0, 0]';
		die();
	}

	$value             = $_GET['value'];
	$page_address      = $_GET['page_address'];
	$voted_cookie_name = md5($page_address).'_voted';

	if (!in_array($value, array('plus', 'minus'))) {
		print '[0, 0]';
		die();
	}

	if (!is_file(SITE_DIR.$page_address)) {
		print '[0, 0]';
		die();
	}

	if (!array_key_exists($voted_cookie_name, $_COOKIE)) {
		vote_page($page_address, $value);
		setcookie($voted_cookie_name, 1, time() + 60 * 60 * 24 * 14, '/'); // 2 недели
	}

	$rating_data = get_rating($page_address);

	print json_encode($rating_data);

?>