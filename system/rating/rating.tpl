      <div itemscope itemtype="http://schema.org/Article" class="vote">
        <div class="vote__rating"><b id="rating"><?=$rating;?></b> из <span>10</span></div>
        <div class="vote__result">Проголосовало <span id="count"><?=$count;?></span></div>
        
        <div id="mainvote" class="vote__poll<?=$voted;?>">
          <span id="plus_b" class="vote__btn vote__btn_plus"></span>
          <span id="minus_b" class="vote__btn vote__btn_minus"></span>
        </div>
		<div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
			<meta itemprop="bestRating" content="10">
			<meta itemprop="ratingValue" content="<?=$rating;?>">
			<meta itemprop="ratingCount" content="<?=$count;?>">
		</div>
      </div>
      <script>
        var cookie_name = '<?=$voted_cookie_name;?>';
      </script>